CREATE DATABASE  IF NOT EXISTS `task38` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `task38`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: task38
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gear`
--

DROP TABLE IF EXISTS `gear`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gear` (
  `id` bigint(22) unsigned NOT NULL AUTO_INCREMENT COMMENT 'идентификатор',
  `brand` varchar(100) NOT NULL COMMENT 'брэнд',
  `model` varchar(100) DEFAULT NULL COMMENT 'модель',
  `width` decimal(3,0) NOT NULL COMMENT 'ширина',
  `height` decimal(3,0) NOT NULL COMMENT 'высота',
  `construction` char(1) CHARACTER SET latin1 NOT NULL COMMENT 'конструкция',
  `diameter` decimal(2,0) unsigned NOT NULL COMMENT 'диаметр',
  `load_index` varchar(10) CHARACTER SET latin1 DEFAULT NULL COMMENT 'индекс загрузки',
  `speed_index` varchar(10) CHARACTER SET latin1 DEFAULT NULL COMMENT 'индекс скорости',
  `chars` varchar(100) DEFAULT NULL COMMENT 'характеризующие аббревиатуры',
  `runflat` varchar(10) CHARACTER SET latin1 DEFAULT NULL COMMENT 'runflat',
  `chamberiness` varchar(10) DEFAULT NULL COMMENT 'камерность',
  `season` varchar(20) DEFAULT NULL COMMENT 'сезонность',
  `studded` tinyint(1) unsigned DEFAULT NULL COMMENT 'шипованность:\n* null = неизвестно;\n* 1 = "да";\n* 0 = "нет".',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COMMENT='шины';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gear`
--

LOCK TABLES `gear` WRITE;
/*!40000 ALTER TABLE `gear` DISABLE KEYS */;
INSERT INTO `gear` VALUES (1,'ToyoH08',NULL,195,75,'R',16,NULL,NULL,'C 107/105STL ',NULL,NULL,'летние',NULL),(2,'Pirelli','Winter SnowControlserie 3',175,70,'R',14,'84','T',NULL,NULL,'TL','зимние',0),(3,'BFGoodrich','Mud-Terrain T/A KM2',235,85,'R',16,'120','1','16Q Внедорожные',NULL,'TL',NULL,NULL),(4,'Pirelli','Scorpion Ice & Snow',265,45,'R',21,'104','H',NULL,NULL,'TL','зимние',0),(5,'Pirelli','Winter SottoZeroSerie II',245,45,'R',19,'102','V','XL   ','RunFlat','TL','зимние',0),(6,'Pirelli','Winter Carving Edge',225,50,'R',17,'98','T','XL  ',NULL,'TL','зимние',1),(7,'Continental','ContiCrossContact LX Sport',255,55,'R',18,'105','H','FR MO ',NULL,'TL','всесезонные',NULL),(8,'BFGoodrich','g-Force Stud',205,60,'R',16,'96','Q','XL  ',NULL,'TL','зимние',1),(9,'BFGoodrich','Winter Slalom KSI',225,60,'R',17,'99','S',NULL,NULL,'TL','зимние',0),(10,'Continental','ContiSportContact 5',245,45,'R',18,'96','W','FR ','SSR','TL','летние',NULL),(11,'Continental','ContiWinterContact TS 830 P',205,60,'R',16,'92','H',NULL,'SSR','TL','зимние',0),(12,'Continental','ContiWinterContact TS 830 P',225,45,'R',18,'95','V','XL FR   ','SSR','TL','зимние',0),(13,'Hankook','Winter I*Cept Evo2 W320',255,35,'R',19,'96','V','XL /TT  ',NULL,'TL','зимние',0);
/*!40000 ALTER TABLE `gear` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `input`
--

DROP TABLE IF EXISTS `input`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `input` (
  `id` bigint(22) unsigned NOT NULL AUTO_INCREMENT COMMENT 'идентификатор',
  `string` longblob NOT NULL COMMENT 'строка',
  `invalid` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'не удалось разобрать:\nудалось=0\nне удалось=1',
  PRIMARY KEY (`id`),
  KEY `idx_invalid` (`invalid`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COMMENT='входные данные';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `input`
--

LOCK TABLES `input` WRITE;
/*!40000 ALTER TABLE `input` DISABLE KEYS */;
INSERT INTO `input` VALUES (1,'ToyoH08 195/75R16C 107/105STL Летние',0),(2,'Pirelli Winter SnowControlserie 3 175/70R14 84T TL Зимние (нешипованные)',0),(3,'BFGoodrich Mud-Terrain T/A KM2 235/85R16 120/116Q TL Внедорожные',0),(4,'Pirelli Scorpion Ice & Snow 265/45R21 104H TL Зимние (нешипованные)',0),(5,'Pirelli Winter SottoZeroSerie II 245/45R19 102V XL Run Flat * TL Зимние (нешипованные)',0),(6,'Nokian Hakkapeliitta R2 SUV/Е245/70R16 111R XL TL Зимние (нешипованные)',1),(7,'Pirelli Winter Carving Edge 225/50R17 98T XL TL Зимние (шипованные)',0),(8,'Continental ContiCrossContact LX Sport 255/55R18 105H FR MO TL Всесезонные',0),(9,'BFGoodrich g-Force Stud 205/60R16 96Q XL TL Зимние (шипованные)',0),(10,'BFGoodrich Winter Slalom KSI 225/60R17 99S TL Зимние (нешипованные)',0),(11,'Continental ContiSportContact 5 245/45R18 96W SSR FR TL Летние',0),(12,'Continental ContiWinterContact TS 830 P 205/60R16 92H SSR * TL Зимние (нешипованные)',0),(13,'Continental ContiWinterContact TS 830 P 225/45R18 95V XL SSR FR * TL Зимние (нешипованные)',0),(14,'Hankook Winter I*Cept Evo2 W320 255/35R19 96V XL TL/TTЗимние (нешипованные)',0),(15,'Mitas Sport Force+ 120/65R17 56W TL Летние',1);
/*!40000 ALTER TABLE `input` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'task38'
--

--
-- Dumping routines for database 'task38'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-25 12:25:47
