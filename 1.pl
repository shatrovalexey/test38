use strict ;
use utf8 ;
use DBI ;

my $dbh = DBI->connect( 'dbi:mysql:task38' , 'root' , 'f2ox9erm' ) ;
$dbh->do( $_ ) foreach << '.' , << '.' ;
SET NAMES utf8mb4 ;
.
TRUNCATE `gear` ;
.

my ( $sth_sel , $sth_ins , $sth_state ) = map $dbh->prepare_cached( $_ ) , << '.' , << '.' , << '.' ;
SELECT
	`i1`.`id` ,
	`i1`.`string`
FROM
	`input` AS `i1` ;
.
INSERT INTO
	`gear`
SET
	`brand` := ? ,
	`model` := ? ,
	`width` := ? ,
	`height` := ? ,
	`construction` := ? ,
	`diameter` := ? ,
	`load_index` := ? ,
	`speed_index` := ? ,
	`chars` := ? ,
	`runflat` := ? ,
	`chamberiness` := ? ,
	`season` := ? ,
	`studded` := ? ;
.
UPDATE
	`input` AS `i1`
SET
	`i1`.`invalid` := ?
WHERE
	( `i1`.`id` = ? ) ;
.

$sth_sel->execute( ) ;

my %rx = (
	'main' => qr(
		(\w+)	\s*
		(?:(.*?))?\b	\s+

		(\d+)
			/
		(\d+)
		([[:alpha:]])
		(\d{2})
		(?:
			\s*
			(\d+)\s*/?\s*
			(\w
				(?:\([\w+]\))?
			)
		)?
	)xsoui ,
	'runflat' => qr(
		\b(?:
			(
				(?i)
					run[\*\s]*flat
				(?-i)
			) | (
				ROF	|
				ZP	|
				SSR	|
				ZPS	|
				HRS	|
				RFT
			)
		)\b
	)xsou ,
	'chamberiness' => qr(
		\b(?:TT|TL|TL/TT)\b
	)xsoui ,
	'season' => qr(
		(?:летние|зимние|всесезонн?ые)\b
	)xsoui ,
	'studded' => qr(
		\b(не\s*)?шипованн?ые\b
	)xsoui
) ;

while ( my ( $id , $chars ) = $sth_sel->fetchrow_array( ) ) {
	utf8::decode( $chars ) ;

	study( $chars ) ;

	do {
		$sth_state->execute( 1 , $id ) ;

		next( ) ;
	} unless $chars =~ s{ $rx{ 'main' } }{}x ;

	my ( $brand , $model , $width , $height , $construction , $diameter , $load_index , $speed_index ) = (
		$1 , $2 , $3 , $4 , $5 , $6 , $7 , $8
	) ;
	my $season = $chars =~ s{ $rx{ 'season' } }{}x ? lc( $& ) : undef( ) ;
	my $studded = $chars =~ s{ $rx{ 'studded' } }{}x ? int( ! length( $1 ) ) : undef( ) ;
	my $runflat = $chars =~ s{ $rx{ 'runflat' } }{}x ? $1 ? 'RunFlat' : $2 : undef( ) ;

	my $chamberiness = $chars =~ s{ $rx{ 'chamberiness' } }{}x ? $& : undef( ) ;

	$chars =~ s{\s+|\(\s*\)}{ }gos ;
	$chars =~ s{^\s+|\s*/\s*$}{}gos ;
	$chars =~ s{\*}{}gos ;

	$sth_state->execute( int( $sth_ins->execute(
		map m{\S}os ? $_ : undef( ) ,
			$brand , $model , $width , $height , $construction , $diameter , $load_index ,
			$speed_index , $chars , $runflat , $chamberiness , $season , $studded
	) <= 0 ) , $id ) ;
}

$dbh->disconnect( ) ;